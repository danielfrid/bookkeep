using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Bookkeeper.model
{
    public class Account:IAccount
    {
        public string Name
        {
            get
            {
                return this.Name;
            }
            set
            {
                this.Name = value;
            }
        }

        public int Number
        {
            get
            {
                return this.Number;
            }
            set
            {
                this.Number = value;
            }
        }
    }
}