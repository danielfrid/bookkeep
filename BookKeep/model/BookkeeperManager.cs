using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Bookkeeper.model
{
    public class BookkeeperManager:IBookkeeperManager
    {
        public BookkeeperManager()
        {
            setAccountsAndRates();
        }
        public List<Account> IncomeAccounts
        {
            get { return IncomeAccounts; }
        }

        public List<Account> ExpenseAccounts
        {
            get { return ExpenseAccounts; }
        }

        public List<Account> MoneyAccounts
        {
            get { return MoneyAccounts; }
        }

        public List<TaxRate> TaxRates
        {
            get { return TaxRates; }
        }

        public List<Entry> Entries
        {
            get { return Entries; }
        }

        public void AddEntry(Entry e)
        {
            Entries.Add(e);
        }

        private void setAccountsAndRates()
        {
            IncomeAccounts.Add(new Account { Name = "F�rs�ljning", Number = 3000 });
            IncomeAccounts.Add(new Account { Name = "F�rs�ljning av tj�nster", Number = 3040 });
            ExpenseAccounts.Add(new Account { Name = "F�rs�ljning av tj�nster", Number = 3040 });
            ExpenseAccounts.Add(new Account() { Name = "�vriga egna uttag", Number = 2013 });
            ExpenseAccounts.Add(new Account() { Name = "Reklam och PR", Number = 5900 });
            MoneyAccounts.Add(new Account() { Name = "Kassa", Number = 1910 });
            MoneyAccounts.Add(new Account() { Name = "F�retagskonto", Number = 1930 });
            MoneyAccounts.Add(new Account() { Name = "Egna ins�ttningar", Number = 2018 });

            TaxRates.Add(new TaxRate { Tax = 25 });
            TaxRates.Add(new TaxRate { Tax = 12 });
            TaxRates.Add(new TaxRate { Tax = 6 });
        
        }
    }
}