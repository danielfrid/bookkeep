using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Bookkeeper
{
    [Activity(Label = "Activity1")]
    public class NewEventActivity : Activity
    {
        private TextView dateDisplay;
        private Button pickDate;
        private DateTime date;
        private RadioButton incomeButt, expenseButt;
        private EditText descriptionText;
        private Spinner account, kassa, vat;
        const int DATE_DIALOG_ID = 0;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);  
            SetContentView(Resource.Layout.activity_new_event);

            incomeButt = FindViewById<RadioButton>(Resource.Id.income_radio);
            expenseButt = FindViewById<RadioButton>(Resource.Id.expense_radio);
            expenseButt.Checked = true;
            expenseButt.Click += UpdateSpinner;
            incomeButt.Click += UpdateSpinner;
            account = FindViewById<Spinner>(Resource.Id.account_spinner);
            kassa = FindViewById<Spinner>(Resource.Id.type_spinner);
            vat = FindViewById<Spinner>(Resource.Id.vat_spinner);
            descriptionText = FindViewById<EditText>(Resource.Id.description_textedit);



            // capture our View elements
            dateDisplay = FindViewById<TextView>(Resource.Id.dateDisplay);
            pickDate = FindViewById<Button>(Resource.Id.pickDate);
            // add a click event handler to the button
            pickDate.Click += delegate { ShowDialog(DATE_DIALOG_ID); };
            // get the current date
            date = DateTime.Today;
            // display the current date (this method is below)
            UpdateDisplay();

        }

        void UpdateSpinner(Object sender, EventArgs e) { }
        // Denna vet jag inte riktigt VAR den skall ligga !!!
        // the event received when the user "sets" the date in the dialog
        void OnDateSet(object sender, DatePickerDialog.DateSetEventArgs e)
        {
            this.date = e.Date;
            UpdateDisplay();
        }
        protected override Dialog OnCreateDialog(int id)
        {
            switch (id)
            {
                case DATE_DIALOG_ID:
                    return new DatePickerDialog(this, OnDateSet, date.Year, date.Month - 1, date.Day);
            }
            return null;
        }
        private void UpdateDisplay()
        {
            dateDisplay.Text = date.ToString("d");
        }
    }
}