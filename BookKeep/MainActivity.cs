﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace Bookkeeper
{
    [Activity(Label = "BookKeep", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
       
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.activity_main);
                   
            Button button = FindViewById<Button>(Resource.Id.NewEventButton);
            Button button2 = FindViewById<Button>(Resource.Id.ShowEventsButton);
            Button button3 = FindViewById<Button>(Resource.Id.MakeReportButton);
            Intent i;
            button.Click += delegate 
                {
                    i = new Intent(this, typeof(NewEventActivity));
                    StartActivity(i);
                };
            button2.Click += delegate
            {
                i = new Intent(this, typeof(ShowEventsActivity));
                StartActivity(i);
            };
            button3.Click += delegate
            {
                i = new Intent(this, typeof(ReportActivity));
                StartActivity(i);
            };
        }
    }
}

